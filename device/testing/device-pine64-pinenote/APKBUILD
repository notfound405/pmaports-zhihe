# Maintainer: Petr Hodina <phodina@protonmail.com>
pkgname=device-pine64-pinenote
pkgdesc="Pine64 PineNote"
pkgver=2
pkgrel=0
url="https://postmarketos.org"
license="MIT"
arch="aarch64"
options="!check !archcheck"
depends="
	u-boot-pine64-pinenote
	linux-pine64-pinenote
	postmarketos-base
"
makedepends="devicepkg-dev"
source="deviceinfo
	phoc.ini
	local-overrides.quirks
	50-touchscreen.conf
	81-libinput-pinenote.rules
	82-ebc-rockchip.rules
"
subpackages="
	$pkgname-nonfree-firmware:nonfree_firmware
	$pkgname-gnome
	$pkgname-phosh
"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname

	install -Dm644 "$srcdir"/local-overrides.quirks \
		-t "$pkgdir"/etc/libinput/
	install -Dm644 "$srcdir"/50-touchscreen.conf \
		-t "$pkgdir"/etc/X11/xorg.conf.d
	install -Dm644 "$srcdir"/81-libinput-pinenote.rules \
		-t "$pkgdir"/lib/udev/rules.d
	install -Dm644 "$srcdir"/82-ebc-rockchip.rules \
		"$pkgdir"/lib/udev/rules.d
}

gnome() {
	install_if="$pkgname=$pkgver-r$pkgrel gnome"
	depends="gnome-shell gnome-shell-extension-pinenote"
	mkdir -p "$subpkgdir"
}

phosh() {
	install_if="$pkgname=$pkgver-r$pkgrel phosh"
	depends="postmarketos-theme"

	install -Dm644 "$srcdir"/phoc.ini \
		-t "$subpkgdir"/etc/phosh
}

nonfree_firmware() {
	pkgdesc="WiFi, Bluetooth and display firmware"
	depends="firmware-pine64-pinenote linux-firmware"
	mkdir "$subpkgdir"
}

sha512sums="
c89fc650361331d0c7fefc444f6170510f56ace4c28ebf1d5a6556af4bf95c96d80c17bc26db0f9eb690f59ab316b48e5523f4720be566617a53fecd0d95c431  deviceinfo
4bf5158fbd53274a7429e825bb66225001f2403a4851e2d6803323b77d9095738ee3e5340ac85baf3e86bb4f47d38af8cbd78d8a5055c59a62f5b06e722e19cb  phoc.ini
1123720962c9c8fec3c50302ca6a3dd56e2907dc9eea361a7b8eb4201b042476633d41a0ee4f6ab61d9c60eeccc894f83491ba9fa309a9bce2f1db0b0341d79d  local-overrides.quirks
ac433eebbc35a48561837495997aee4e55510b979bc0d8e3bafb761bc1be5b4bdeed2f456369dcbc582688aefd07c63966b0d72b6ffa99e84cfd868e677f02c8  50-touchscreen.conf
2bc51f200baefc37abfaaad368a911244999e906bdca4b728ac233f49a8fb3ae7206ee3c95cdb20d7dceae2a31d25a57f4e1da4fd67057fd64724b8232e42aed  81-libinput-pinenote.rules
7c418cebf336df96cd484d7f0243cf27e859b11a7f2e7fb54dd2b4afedc4f570588bee08a5de8075db7c7d6d56b7971d19dc09909ebe68247505a3e37427e312  82-ebc-rockchip.rules
"
